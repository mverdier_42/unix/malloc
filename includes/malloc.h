/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 11:20:15 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 16:52:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <stdlib.h>
# include <sys/mman.h>
# include <unistd.h>
# include <pthread.h>

# define P_FLAGS PROT_READ | PROT_WRITE
# define M_FLAGS MAP_ANON | MAP_PRIVATE
# define TINY_SIZE (size_t)getpagesize()
# define SMALL_SIZE (size_t)(32 * getpagesize())
# define TINY_MAX (TINY_SIZE / 100)
# define SMALL_MAX (SMALL_SIZE / 100)

typedef struct	s_blocks
{
	void			*ptr;
	size_t			size;
	int				is_free;
	struct s_blocks	*prev;
	struct s_blocks	*next;
}				t_blocks;

typedef struct	s_pool
{
	void			*pool;
	size_t			size;
	t_blocks		*allocs;
	struct s_pool	*prev;
	struct s_pool	*next;
}				t_pool;

typedef struct	s_pages
{
	size_t			size;
	struct s_pages	*next;
}				t_pages;

typedef struct	s_malloc
{
	t_pages			*page;
	t_pool			*tiny_pools;
	t_pool			*small_pools;
	t_blocks		*larges;
}				t_malloc;

t_malloc		g_malloc;
pthread_mutex_t	g_mutex;

void			*malloc(size_t size);
void			*realloc(void *ptr, size_t size);
void			free(void *ptr);
void			show_alloc_mem(void);
void			*calloc(size_t count, size_t size);
void			*valloc(size_t size);

void			*tiny_malloc(size_t size);
void			*small_malloc(size_t size);
void			*large_malloc(size_t size);
t_blocks		*get_free_space(t_pool **pool, t_blocks **block, size_t size,
				size_t max);
t_pages			*get_last_page(void);
void			add_pool(t_pool **pool, size_t pool_size);
void			*mmap_failed(void);
void			print_ptr_to_hex(void *ptr);
int				free_ptr(void *ptr, t_blocks *block, t_pool **pool,
				size_t size);
int				realloc_page(t_pages **page);
t_blocks		*create_block(t_pool **pool, t_blocks **tmp, size_t size,
				size_t pool_size);

#endif
