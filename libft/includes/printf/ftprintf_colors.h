/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_colors.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:44:44 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:23:08 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_COLORS_H
# define FTPRINTF_COLORS_H

# define BLACK		"\033[1;30m"
# define RED 		"\033[1;31m"
# define GREEN		"\033[1;32m"
# define YELLOW		"\033[1;33m"
# define BLUE		"\033[1;34m"
# define PURPLE		"\033[1;35m"
# define CYAN		"\033[1;36m"
# define GREY		"\033[1;37m"
# define LBLACK		"\033[0;30m"
# define LRED 		"\033[0;31m"
# define LGREEN		"\033[0;32m"
# define LYELLOW	"\033[0;33m"
# define LBLUE		"\033[0;34m"
# define LPURPLE	"\033[0;35m"
# define LCYAN		"\033[0;36m"
# define LGREY		"\033[0;37m"
# define RES		"\033[0;m"

#endif
