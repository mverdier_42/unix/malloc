/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strextract.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 15:29:28 by mverdier          #+#    #+#             */
/*   Updated: 2017/03/15 15:31:08 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strextract(char *str, char c)
{
	char	*tmp;

	if (!(tmp = ft_strchr(str, c)))
		return (ft_strdup(str));
	return (ft_strsub(str, 0, tmp - str));
}
