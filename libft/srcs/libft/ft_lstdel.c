/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 19:20:20 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/08 20:08:11 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

void	ft_lstdel(t_list **alst, void (*del)(void *))
{
	t_list	*tmp;
	t_list	*temp;

	tmp = *alst;
	while (tmp)
	{
		temp = tmp->next;
		(*del)(tmp->content);
		free(tmp);
		tmp = temp;
	}
}
