/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:51:55 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/05 17:28:02 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

static void	ft_cmp(const char *big, const char *little, size_t *i, size_t *j)
{
	while (big[*i] == little[*j] && big[*i] && little[*j])
	{
		(*i)++;
		(*j)++;
	}
}

char		*ft_strstr(const char *big, const char *little)
{
	size_t i;
	size_t j;
	size_t k;

	i = 0;
	if (little[i] == '\0')
		return ((char *)big);
	while (big[i])
	{
		j = 0;
		while (big[i] && big[i] != little[j])
			i++;
		if (big[i] == '\0')
			return (NULL);
		k = i;
		ft_cmp(big, little, &i, &j);
		if (little[j])
			i = k + 1;
		else
			return ((char *)big + k);
	}
	return (NULL);
}
