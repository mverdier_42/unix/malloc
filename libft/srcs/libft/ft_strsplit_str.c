/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 15:19:38 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/02 16:16:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

static int		ft_countword(char const *s, char *chars)
{
	int i;
	int w;

	i = 0;
	w = 0;
	while (s[i])
	{
		while (ft_strchr(chars, s[i]))
			i++;
		while (s[i] && !ft_strchr(chars, s[i]))
		{
			i++;
			if (ft_strchr(chars, s[i]) || s[i] == '\0')
				w++;
		}
	}
	return (w);
}

static char		**ft_wordalloc(char const *s, char *chars, char **newtab,
		int word)
{
	int i;
	int len;
	int w;

	i = 0;
	w = 0;
	while (s[i] && w < word)
	{
		while (ft_strchr(chars, s[i]))
			i++;
		len = 0;
		while (s[i] && !ft_strchr(chars, s[i]))
		{
			i++;
			len++;
			if (ft_strchr(chars, s[i]) || s[i] == '\0')
			{
				if ((newtab[w] = (char*)malloc(sizeof(char) * len + 1)) == NULL)
					return (NULL);
				w++;
			}
		}
	}
	return (newtab);
}

static char		**ft_fillit(char const *s, char *chars, char **newtab, int word)
{
	int i;
	int j;
	int w;

	i = 0;
	w = 0;
	while (s[i] && w < word)
	{
		while (s[i] && ft_strchr(chars, s[i]))
			i++;
		j = 0;
		while (s[i] && !ft_strchr(chars, s[i]))
		{
			newtab[w][j] = s[i];
			i++;
			j++;
			if (ft_strchr(chars, s[i]) || s[i] == '\0')
			{
				newtab[w][j] = '\0';
				w++;
			}
		}
	}
	newtab[w] = NULL;
	return (newtab);
}

char			**ft_strsplit_str(char const *s, char *chars)
{
	char	**newtab;
	int		word;

	if (!s)
		return (NULL);
	word = ft_countword(s, chars);
	if ((newtab = (char **)malloc(sizeof(char*) * (word + 1))) == NULL)
		return (NULL);
	if ((newtab = ft_wordalloc(s, chars, newtab, word)) == NULL)
		return (NULL);
	newtab = ft_fillit(s, chars, newtab, word);
	return (newtab);
}
