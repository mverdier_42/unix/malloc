/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:51:45 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/03 17:47:30 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

static int		ft_countword(char const *s, char c)
{
	int i;
	int w;

	i = 0;
	w = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		while (s[i] && s[i] != c)
		{
			i++;
			if (s[i] == c || s[i] == '\0')
				w++;
		}
	}
	return (w);
}

static char		**ft_wordalloc(char const *s, char c, char **newtab, int word)
{
	int i;
	int len;
	int w;

	i = 0;
	w = 0;
	while (s[i] && w < word)
	{
		while (s[i] == c)
			i++;
		len = 0;
		while (s[i] && s[i] != c)
		{
			i++;
			len++;
			if (s[i] == c || s[i] == '\0')
			{
				if ((newtab[w] = (char*)malloc(sizeof(char) * len + 1)) == NULL)
					return (NULL);
				w++;
			}
		}
	}
	return (newtab);
}

static char		**ft_fillit(char const *s, char c, char **newtab, int word)
{
	int i;
	int j;
	int w;

	i = 0;
	w = 0;
	while (s[i] && w < word)
	{
		while (s[i] && s[i] == c)
			i++;
		j = 0;
		while (s[i] && s[i] != c)
		{
			newtab[w][j] = s[i];
			i++;
			j++;
			if (s[i] == c || s[i] == '\0')
			{
				newtab[w][j] = '\0';
				w++;
			}
		}
	}
	newtab[w] = NULL;
	return (newtab);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**newtab;
	int		word;

	if (!s)
		return (NULL);
	word = ft_countword(s, c);
	if ((newtab = (char **)malloc(sizeof(char*) * (word + 1))) == NULL)
		return (NULL);
	if ((newtab = ft_wordalloc(s, c, newtab, word)) == NULL)
		return (NULL);
	newtab = ft_fillit(s, c, newtab, word);
	return (newtab);
}
