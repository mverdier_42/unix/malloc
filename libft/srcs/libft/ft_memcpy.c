/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:45:18 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/05 20:25:47 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char	*d;

	d = dest;
	while (n > 0)
	{
		*d = *(unsigned char *)src;
		n--;
		if (n > 0)
		{
			d++;
			src++;
		}
	}
	return (dest);
}
