/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_b_new.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 17:30:51 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/11 14:13:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

t_dlist_b	*ft_dlst_b_new(void)
{
	t_dlist_b	*dlist_b;

	if ((dlist_b = (t_dlist_b*)malloc(sizeof(t_dlist_b))) == NULL)
		return (NULL);
	dlist_b->first = NULL;
	dlist_b->last = NULL;
	dlist_b->size = 0;
	return (dlist_b);
}
