# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/14 14:55:01 by mverdier          #+#    #+#              #
#    Updated: 2018/10/27 16:06:56 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

ORANGE =	\033[1;33m   #It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = 		libft_malloc_$(HOSTTYPE).so

SRCDIR =	./srcs

OBJDIR =	./objs

INCDIR =	./includes

# List all sources, objects and includes files of 'libft'.

SRC =		malloc.c calloc.c tiny_malloc.c small_malloc.c large_malloc.c	\
			get_free_space.c get_last_page.c add_pool.c	realloc.c	\
			show_alloc_mem.c free.c valloc.c mmap_failed.c free_ptr.c	\
			print_ptr_to_hex.c realloc_page.c create_block.c

INC =		malloc.h

SRCS =		$(SRC:%=$(SRCDIR)/%)

OBJS =		$(SRC:%.c=$(OBJDIR)/%.o)

INCS =		$(INC:%=$(INCDIR)/%)

LIBDIR =	./libft

LIBNAME =	libft.a

LIB =		$(LIBDIR)/$(LIBNAME)

LIBINC =	$(LIBDIR)/includes

#------------------------------------------------------------------------------#
# List all compilation variables.

CC =		gcc

CFLAGS =	-Wall			\
			-Wextra			\
			-Werror			\

INCFLAGS =	-I $(INCDIR)	\
			-I $(LIBINC)

LFLAGS =	-L $(LIBDIR) -l$(LIBNAME:lib%.a=%)

FLAGS =		$(CFLAGS)		\
			$(INCFLAGS)

#------------------------------------------------------------------------------#
# List all rules used to make libft.a

all:
	@$(MAKE) -C $(LIBDIR)
	@$(MAKE) $(NAME)

$(NAME): $(OBJS) $(LIB)
	@$(MAKE) printname
	@printf "%-15s%s\n" Linking $@
	@$(CC) $(CFLAGS) $^ -shared -o $@ $(LFLAGS)
	@ln -fs $(NAME) libft_malloc.so
	@printf "$(GREEN)"
	@echo "Compilation done !"
	@printf "$(RES)"

$(OBJS): $(INCS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c -fPIC $<

printname:
	@printf "$(ORANGE)"
	@printf "[%-15s " "$(NAME)]"
	@printf "$(RES)"

clean:
	@$(MAKE) printname
	@echo Suppressing obj files
	@printf "$(RED)"
	rm -rf $(OBJS)
	@rm -rf $(OBJDIR)
	@printf "$(RES)"

fclean: clean
	@$(MAKE) printname
	@echo Suppressing $(NAME)
	@printf "$(RED)"
	rm -rf $(NAME)
	@printf "$(RES)"

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

NORM = `norminette $(SRCS) $(INCS) | grep -B1 Error | cat`

norm:
	@$(MAKE) printname
	@echo "Passage de la norminette :"
	@if [ "$(NORM)" == "`echo ""`" ];									\
		then															\
			echo "$(GREEN)Tous les fichiers sont a la norme !$(RES)";	\
		else															\
			echo "$(RED)$(NORM)$(RES)";									\
	fi

check:
	#@$(MAKE) -C $(LIBDIR) check
	@$(MAKE) norm

# A rule to make git add easier

git:
	@$(MAKE) -C $(LIBDIR) git
	@$(MAKE) printname
	@echo Adding files to git repository
	@printf "$(GREEN)"
	git add $(SRCS) $(INCS) Makefile
	@printf "$(RES)"
	git status

.PHONY: all clean re fclean git norm check
