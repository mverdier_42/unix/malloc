/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 15:07:51 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:43:01 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static int	print_block(t_blocks *tmp)
{
	ft_putstr("0x");
	print_ptr_to_hex(tmp->ptr);
	ft_putstr(" - 0x");
	print_ptr_to_hex((void*)((char*)tmp->ptr + tmp->size - 1));
	ft_putstr(" : ");
	ft_putnbr(tmp->size);
	ft_putstr(" octets\n");
	return (tmp->size);
}

static int	print_tiny_memory(void)
{
	t_pool		*pool_tmp;
	t_blocks	*tmp;
	size_t		total_size;

	total_size = 0;
	pool_tmp = g_malloc.tiny_pools;
	ft_putstr("TINY : 0x");
	if (g_malloc.tiny_pools != NULL)
		print_ptr_to_hex(g_malloc.tiny_pools->pool);
	else
		ft_putstr("0");
	ft_putchar('\n');
	while (pool_tmp != NULL)
	{
		tmp = pool_tmp->allocs;
		while (tmp != NULL)
		{
			if (tmp->is_free == 0)
				total_size += print_block(tmp);
			tmp = tmp->next;
		}
		pool_tmp = pool_tmp->next;
	}
	return (total_size);
}

static int	print_small_memory(void)
{
	t_pool		*pool_tmp;
	t_blocks	*tmp;
	size_t		total_size;

	total_size = 0;
	pool_tmp = g_malloc.small_pools;
	ft_putstr("SMALL : 0x");
	if (g_malloc.small_pools != NULL)
		print_ptr_to_hex(g_malloc.small_pools->pool);
	else
		ft_putstr("0");
	ft_putchar('\n');
	while (pool_tmp != NULL)
	{
		tmp = pool_tmp->allocs;
		while (tmp != NULL)
		{
			if (tmp->is_free == 0)
				total_size += print_block(tmp);
			tmp = tmp->next;
		}
		pool_tmp = pool_tmp->next;
	}
	return (total_size);
}

static int	print_large_memory(void)
{
	t_blocks	*tmp;
	size_t		total_size;

	total_size = 0;
	tmp = g_malloc.larges;
	ft_putstr("LARGE : 0x");
	if (g_malloc.larges != NULL)
		print_ptr_to_hex(g_malloc.larges->ptr);
	else
		ft_putstr("0");
	ft_putchar('\n');
	while (tmp != NULL)
	{
		if (tmp->is_free == 0)
			total_size += print_block(tmp);
		tmp = tmp->next;
	}
	return (total_size);
}

void		show_alloc_mem(void)
{
	size_t	total_size;

	total_size = 0;
	total_size += print_tiny_memory();
	total_size += print_small_memory();
	total_size += print_large_memory();
	ft_putstr("Total : ");
	ft_putnbr(total_size);
	ft_putstr(" octets\n");
}
