/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_block.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 15:55:42 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 16:25:12 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

t_blocks	*create_block(t_pool **pool, t_blocks **tmp, size_t size,
	size_t pool_size)
{
	t_blocks	*block;
	t_pages		*page;

	if ((*pool)->size + size > pool_size)
	{
		add_pool(pool, pool_size);
		return (NULL);
	}
	page = get_last_page();
	if (page->size + sizeof(t_blocks) > (size_t)getpagesize())
	{
		if (!realloc_page(&page))
			return (mmap_failed());
	}
	block = (void*)((char*)page + page->size);
	page->size += sizeof(t_blocks);
	block->ptr = (void*)((char*)(*pool)->pool + (*pool)->size);
	(*pool)->size += size;
	block->size = size;
	block->is_free = 0;
	block->next = NULL;
	block->prev = *tmp;
	(*tmp)->next = block;
	return (block);
}
