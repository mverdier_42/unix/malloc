/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_last_page.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/21 17:18:24 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:16:28 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

t_pages		*get_last_page(void)
{
	t_pages		*page;

	if (!g_malloc.page)
	{
		if ((g_malloc.page = mmap(NULL, getpagesize(), P_FLAGS, M_FLAGS,
			-1, 0)) == MAP_FAILED)
			return (NULL);
		g_malloc.page->size = sizeof(t_pages);
		page = g_malloc.page;
		page->next = NULL;
	}
	page = g_malloc.page;
	while (page->next)
		page = page->next;
	return (page);
}
