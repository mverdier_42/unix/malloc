/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 11:19:48 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/26 16:37:42 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

void	*malloc(size_t size)
{
	if (!g_malloc.page)
		pthread_mutex_init(&g_mutex, NULL);
	if (size < 1)
		return (NULL);
	if (size <= TINY_MAX)
		return (tiny_malloc(size));
	if (size <= SMALL_MAX)
		return (small_malloc(size));
	return (large_malloc(size));
}
