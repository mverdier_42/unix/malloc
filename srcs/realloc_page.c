/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc_page.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 15:29:20 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:32:14 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

int		realloc_page(t_pages **page)
{
	if (((*page)->next = mmap(NULL, getpagesize(), P_FLAGS, M_FLAGS,
		-1, 0)) == MAP_FAILED)
		return (0);
	*page = (*page)->next;
	(*page)->size = sizeof(t_pages);
	(*page)->next = NULL;
	return (1);
}
