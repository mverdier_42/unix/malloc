/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ptr_to_hex.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 16:57:23 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:18:18 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static char	*ptr_to_hex(unsigned int n, int *i, char str[])
{
	char			base_str[17];

	ft_strcpy(base_str, "0123456789ABCDEF");
	if (n < 16)
	{
		str[*i] = base_str[n];
		(*i)++;
	}
	if (n >= 16)
	{
		str = ptr_to_hex(n / 16, i, str);
		str[*i] = base_str[n % 16];
		(*i)++;
	}
	return (str);
}

void		print_ptr_to_hex(void *ptr)
{
	char			base_str[17];
	char			str[9];
	unsigned int	n;
	int				i;
	char			*res;

	ft_strcpy(base_str, "0123456789ABCDEF");
	ft_strcpy(str, "\0\0\0\0\0\0\0\0");
	n = (unsigned int)ptr;
	i = 0;
	res = ptr_to_hex(n, &i, str);
	i = ft_strlen(res);
	while (i < 8)
	{
		ft_putchar('0');
		i++;
	}
	ft_putstr(res);
}
