/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_free_space.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/20 13:47:36 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:36:01 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static t_blocks	*use_free_space(t_pool **pool, t_blocks **block, size_t size)
{
	(void)size;
	(*block)->is_free = 0;
	(*pool)->size += (*block)->size;
	return (*block);
}

static t_blocks	*check_blocks(t_pool **pool, t_blocks **block, size_t size)
{
	if (!*block)
		return (NULL);
	while ((*block)->next)
	{
		if ((*block)->is_free == 1 && (*block)->size >= size)
			return (use_free_space(pool, block, size));
		*block = (*block)->next;
	}
	if (*block != NULL && (*block)->is_free == 1
		&& (*block)->size >= size)
		return (use_free_space(pool, block, size));
	return (NULL);
}

t_blocks		*get_free_space(t_pool **pool, t_blocks **block, size_t size,
	size_t max)
{
	t_blocks	*tmp;

	while ((*pool)->next)
	{
		if ((*pool)->size + size > max)
		{
			*pool = (*pool)->next;
			continue ;
		}
		*block = (*pool)->allocs;
		if ((tmp = check_blocks(pool, block, size)) != NULL)
			return (tmp);
		*pool = (*pool)->next;
	}
	if ((*pool)->size + size > max)
		return (NULL);
	*block = (*pool)->allocs;
	if ((tmp = check_blocks(pool, block, size)) != NULL)
		return (tmp);
	return (NULL);
}
