/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 11:20:02 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:37:09 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static void	*realloc_large(void *ptr, size_t size)
{
	void		*new_ptr;
	t_blocks	*block;

	block = g_malloc.larges;
	while (block)
	{
		if (ptr == block->ptr)
		{
			if ((new_ptr = malloc(size)) == NULL)
				return (NULL);
			ft_memcpy(new_ptr, ptr, (block->size < size ? block->size : size));
			munmap(ptr, block->size);
			return (new_ptr);
		}
	}
	return (NULL);
}

static void	*realloc_small(void *ptr, size_t size)
{
	void		*new_ptr;
	t_pool		*pool;
	t_blocks	*block;

	pool = g_malloc.small_pools;
	while (pool)
	{
		block = pool->allocs;
		while (block)
		{
			if (ptr == block->ptr)
			{
				if ((new_ptr = malloc(size)) == NULL)
					return (NULL);
				ft_memcpy(new_ptr, ptr, (block->size < size ?
					block->size : size));
				block->is_free = 1;
				return (new_ptr);
			}
			block = block->next;
		}
		pool = pool->next;
	}
	return (NULL);
}

static void	*realloc_tiny(void *ptr, size_t size)
{
	void		*new_ptr;
	t_pool		*pool;
	t_blocks	*block;

	pool = g_malloc.tiny_pools;
	while (pool)
	{
		block = pool->allocs;
		while (block)
		{
			if (ptr == block->ptr)
			{
				if ((new_ptr = malloc(size)) == NULL)
					return (NULL);
				ft_memcpy(new_ptr, ptr, (block->size < size ?
					block->size : size));
				block->is_free = 1;
				return (new_ptr);
			}
			block = block->next;
		}
		pool = pool->next;
	}
	return (NULL);
}

void		*realloc(void *ptr, size_t size)
{
	void		*new_ptr;

	if (ptr == NULL)
		return (malloc(size));
	if (size > 0)
	{
		pthread_mutex_lock(&g_mutex);
		if ((new_ptr = realloc_tiny(ptr, size)) != NULL)
		{
			pthread_mutex_unlock(&g_mutex);
			return (new_ptr);
		}
		if ((new_ptr = realloc_small(ptr, size)) != NULL)
		{
			pthread_mutex_unlock(&g_mutex);
			return (new_ptr);
		}
		if ((new_ptr = realloc_large(ptr, size)) != NULL)
		{
			pthread_mutex_unlock(&g_mutex);
			return (new_ptr);
		}
	}
	return (NULL);
}
