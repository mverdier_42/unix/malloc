/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   large_malloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/20 13:47:23 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:36:24 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static void	*create_large(size_t size)
{
	t_blocks	*large;
	t_pages		*page;

	page = get_last_page();
	if (page->size + sizeof(t_blocks) > (size_t)getpagesize())
	{
		if ((page->next = mmap(NULL, getpagesize(), P_FLAGS, M_FLAGS,
			-1, 0)) == MAP_FAILED)
			return (mmap_failed());
		page = page->next;
		page->size = sizeof(t_pages);
		page->next = NULL;
	}
	large = (void*)((char*)page + page->size);
	page->size += sizeof(t_blocks);
	if ((large->ptr = mmap(NULL, size, P_FLAGS, M_FLAGS, -1, 0))
		== MAP_FAILED)
		return (mmap_failed());
	large->size = size;
	large->is_free = 0;
	large->next = NULL;
	large->prev = NULL;
	g_malloc.larges = large;
	pthread_mutex_unlock(&g_mutex);
	return (large->ptr);
}

static void	*add_large(size_t size)
{
	t_blocks	*tmp;
	t_blocks	*large;
	t_pages		*page;

	page = get_last_page();
	tmp = g_malloc.larges;
	while (tmp->next)
		tmp = tmp->next;
	if (page->size + sizeof(t_blocks) > (size_t)getpagesize())
	{
		if (!realloc_page(&page))
			return (mmap_failed());
	}
	large = (void*)((char*)page + page->size);
	page->size += sizeof(t_blocks);
	if ((large->ptr = mmap(NULL, size, P_FLAGS, M_FLAGS, -1, 0))
		== MAP_FAILED)
		return (mmap_failed());
	large->size = size;
	large->is_free = 0;
	large->next = NULL;
	large->prev = tmp;
	tmp->next = large;
	pthread_mutex_unlock(&g_mutex);
	return (large->ptr);
}

void		*large_malloc(size_t size)
{
	if (!g_malloc.larges)
		return (create_large(size));
	return (add_large(size));
}
