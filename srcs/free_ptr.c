/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_ptr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 15:20:28 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:35:18 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static void	free_pool(t_pool **pool, size_t size)
{
	t_pool		*pool_prev;
	t_pool		*pool_next;

	if (munmap((*pool)->pool, size) < 0)
		return ;
	(*pool)->pool = NULL;
	(*pool)->allocs = NULL;
	pool_prev = (*pool)->prev;
	pool_next = (*pool)->next;
	ft_bzero((*pool), sizeof(t_pool));
	(*pool) = NULL;
	if (pool_prev)
		pool_prev->next = pool_next;
	if (pool_next)
		pool_next->prev = pool_prev;
}

int			free_ptr(void *ptr, t_blocks *block, t_pool **pool, size_t size)
{
	t_blocks	*tmp;

	tmp = block;
	while (tmp != NULL)
	{
		if (ptr == tmp->ptr)
		{
			if (tmp->is_free == 1)
				return (1);
			tmp->is_free = 1;
			(*pool)->size -= tmp->size;
			if ((*pool)->size == 0 && ((*pool)->prev != NULL
				|| (*pool)->next != NULL))
				free_pool(pool, size);
			return (1);
		}
		tmp = tmp->next;
	}
	return (0);
}
