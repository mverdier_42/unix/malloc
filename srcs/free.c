/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 11:49:09 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:35:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static bool	free_large_ptr(void *ptr, t_blocks *block)
{
	t_blocks	*tmp;
	t_blocks	*large_prev;
	t_blocks	*large_next;
	t_pages		*page;

	tmp = block;
	while (tmp != NULL)
	{
		if (ptr == tmp->ptr)
		{
			if (munmap(tmp->ptr, tmp->size) < 0)
				return (true);
			large_prev = tmp->prev;
			large_next = tmp->next;
			if (large_prev)
				large_prev->next = large_next;
			if (large_next)
				large_next->prev = large_prev;
			page = get_last_page();
			return (true);
		}
		tmp = tmp->next;
	}
	return (false);
}

static bool	free_small(void *ptr)
{
	t_pool	**pool;

	pool = &g_malloc.small_pools;
	while ((*pool) != NULL)
	{
		if (ptr >= (*pool)->pool && ptr < (*pool)->pool + (*pool)->size)
			return (free_ptr(ptr, (*pool)->allocs, pool, SMALL_SIZE));
		*pool = (*pool)->next;
	}
	return (false);
}

static bool	free_tiny(void *ptr)
{
	t_pool	*pool;

	pool = g_malloc.tiny_pools;
	while (pool != NULL)
	{
		if (ptr >= pool->pool && ptr < pool->pool + pool->size)
			return (free_ptr(ptr, pool->allocs, &pool, TINY_SIZE));
		pool = pool->next;
	}
	return (false);
}

void		free(void *ptr)
{
	if (ptr == NULL)
		return ;
	if (free_tiny(ptr) == true)
		return ;
	if (free_small(ptr) == true)
		return ;
	if (!free_large_ptr(ptr, g_malloc.larges))
		return ;
}
