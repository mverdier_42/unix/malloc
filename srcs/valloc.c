/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:43:27 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 15:17:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

void	*valloc(size_t size)
{
	size_t	new_size;

	new_size = size / getpagesize();
	if (size % getpagesize() != 0)
		new_size++;
	return (malloc(new_size));
}
