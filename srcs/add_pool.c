/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_pool.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/21 18:55:03 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/24 16:51:38 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

void	add_pool(t_pool **pool, size_t pool_size)
{
	t_pool	*new_pool;
	t_pages	*page;

	page = get_last_page();
	if (page->size + sizeof(t_pool) > (size_t)getpagesize())
	{
		if ((page->next = mmap(NULL, getpagesize(), P_FLAGS, M_FLAGS,
			-1, 0)) == MAP_FAILED)
			return ;
		page = page->next;
		page->size = sizeof(t_pages);
	}
	new_pool = (void*)((char*)page + page->size);
	page->size += sizeof(t_pool);
	(*pool)->next = new_pool;
	(*pool)->next->prev = *pool;
	*pool = (*pool)->next;
	(*pool)->size = 0;
	if (((*pool)->pool = mmap(NULL, pool_size, P_FLAGS, M_FLAGS,
		-1, 0)) == MAP_FAILED)
		return ;
}
