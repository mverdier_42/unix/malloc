/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tiny_malloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/20 13:46:58 by mverdier          #+#    #+#             */
/*   Updated: 2018/10/27 16:35:09 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

static void	*create_tinys(t_pool *pool, size_t size)
{
	t_blocks	*block;
	t_pages		*page;

	page = get_last_page();
	if (page->size + sizeof(t_blocks) > (size_t)getpagesize())
	{
		if (!realloc_page(&page))
			return (mmap_failed());
	}
	block = (void*)((char*)page + page->size);
	page->size += sizeof(t_blocks);
	block->ptr = pool->pool;
	pool->size = size;
	block->size = size;
	block->is_free = 0;
	block->prev = NULL;
	block->next = NULL;
	pool->allocs = block;
	pthread_mutex_unlock(&g_mutex);
	return (block->ptr);
}

static void	*add_tiny(size_t size)
{
	t_blocks	*tmp;
	t_blocks	*block;
	t_pool		*pool;

	pool = g_malloc.tiny_pools;
	tmp = g_malloc.tiny_pools->allocs;
	if ((block = get_free_space(&pool, &tmp, size, TINY_SIZE)) != NULL)
	{
		pthread_mutex_unlock(&g_mutex);
		return (block->ptr);
	}
	if ((block = create_block(&pool, &tmp, size, TINY_SIZE)) == NULL)
		return (create_tinys(pool, size));
	pthread_mutex_unlock(&g_mutex);
	return (block->ptr);
}

static bool	create_tiny_pool(void)
{
	t_pages		*page;

	page = get_last_page();
	if (page->size + sizeof(t_pool) > (size_t)getpagesize())
	{
		if (!realloc_page(&page))
			return (false);
	}
	g_malloc.tiny_pools = (void*)((char*)page + page->size);
	g_malloc.tiny_pools->size = 0;
	page->size += sizeof(t_pool);
	g_malloc.tiny_pools->prev = NULL;
	g_malloc.tiny_pools->next = NULL;
	if ((g_malloc.tiny_pools->pool = mmap(NULL, TINY_SIZE, P_FLAGS, M_FLAGS,
		-1, 0)) == MAP_FAILED)
		return (false);
	return (true);
}

void		*tiny_malloc(size_t size)
{
	if (!g_malloc.tiny_pools)
	{
		if (!g_malloc.page)
		{
			if ((g_malloc.page = mmap(NULL, getpagesize(), P_FLAGS, M_FLAGS,
				-1, 0)) == MAP_FAILED)
				return (mmap_failed());
			g_malloc.page->size = sizeof(t_pages);
			g_malloc.page->next = NULL;
		}
		if (!create_tiny_pool())
			return (mmap_failed());
		return (create_tinys(g_malloc.tiny_pools, size));
	}
	return (add_tiny(size));
}
